

/* STYLE_NOTES :
 * Always include the timescale directive for verilog design files
 */
`timescale 1ns / 1ps

/* DESIGN_NOTES :
This module is for the generation of timeout to avoid the scenario of bus getting stalled due to irresponive slaves
 */

module timeout_gen
(
    clk,
    reset,
	
	//Arbiter interface
    timeout_out,

	//SLave Interface
    slave_resp_valid_in,
    
    //Arbiter interface
    arbiter_resp_valid_in,
    
    //Address_decoder
    ad_cs_in
    //timeout_out,                     ** same signal as above

);

//------------------------------------------------
// Global Includes
//------------------------------------------------

`include "bus_global_params.v"

//------------------------------------------------
// Parameters
//------------------------------------------------

    parameter   TIMEOUT_THRESHOLD       = 20;

    localparam  STATE_IDLE              = 1;
    localparam  STATE_COUNTER_SLAVE1    = 2;
    localparam  STATE_COUNTER_SLAVE2    = 3;
    localparam  STATE_COUNTER_SLAVE3    = 4;
    localparam  STATE_WAIT              = 5;
    localparam  STATE_ERROR             = 6;    


//------------------------------------------------
// I/O Signals
//-----------------------------------------------

    input                                                           clk;
    input                                                           reset;
    
    output                                                          timeout_out;
    
    input       [NUMBER_OF_SLAVES - 1:0]                            ad_cs_in;
    input                                                           arbiter_resp_valid_in;
    
    input       [NUMBER_OF_SLAVES - 1:0]                            slave_resp_valid_in;
    


//-----------------------------------------------
// Internal Wires and Regs
//-----------------------------------------------  

    integer                                                         state;
    integer                                                             i;
    reg                                                             timeout_out_reg;
        

//-----------------------------------------------
// Implementation
//-----------------------------------------------  
    always @(posedge clk or posedge reset) begin : main_fsm
    
        if(reset) begin
            state <= STATE_IDLE;
            timeout_out_reg <= 1'b0;
            i <= 0;
        end
        else begin
            case(state)
                STATE_IDLE : begin
                    timeout_out_reg <= 1'b0;
                    if(|ad_cs_in) begin
                        case(ad_cs_in)
                            3'b100 : begin
                                state <= STATE_COUNTER_SLAVE1;
                            end
                            3'b010 : begin
                                state <= STATE_COUNTER_SLAVE2;
                            end
                            3'b001 : begin
                                state <= STATE_COUNTER_SLAVE3;
                            end
                            default : begin
                                state <= STATE_ERROR;
                            end
                        endcase
                    end
                end
                STATE_COUNTER_SLAVE1 : begin
                    if(slave_resp_valid_in[2]) begin
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else if( i == TIMEOUT_THRESHOLD - 1 ) begin
                        timeout_out_reg <= 1'b1;
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else begin
                        i <= i + 1;
                    end
                end
                STATE_COUNTER_SLAVE2 : begin
                    if(slave_resp_valid_in[1]) begin
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else if( i == TIMEOUT_THRESHOLD - 1 ) begin
                        timeout_out_reg <= 1'b1;
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else begin
                        i <= i + 1;
                    end
                end
                STATE_COUNTER_SLAVE3 : begin
                    if(slave_resp_valid_in[0]) begin
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else if( i == TIMEOUT_THRESHOLD - 1 ) begin
                        timeout_out_reg <= 1'b1;
                        state <= STATE_WAIT;
                        i <= 0;
                    end
                    else begin
                        i <= i + 1;
                    end
                end
                STATE_WAIT : begin
                    timeout_out_reg <= 1'b0;
                    state <= STATE_IDLE;
                end
                STATE_ERROR : begin
                    state <= STATE_IDLE;
                end
            endcase
        end
    end
    
    assign timeout_out = timeout_out_reg & (~arbiter_resp_valid_in);

endmodule
