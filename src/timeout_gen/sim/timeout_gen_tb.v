`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   08:16:01 07/29/2013
// Design Name:   timeout_gen
// Module Name:   J:/Campus Electronics/ADSL/bus_design/src/timeout_gen/sim/timeout_gen_tb.v
// Project Name:  bus_09
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: timeout_gen
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module timeout_gen_tb;

	// Inputs
	reg clk;
	reg reset;
	reg [2:0] ad_cs_in;
	reg [2:0] slave_resp_valid_in;
    reg arbiter_resp_valid_in;

	// Outputs
	wire timeout_out;
    wire ad_resp_valid_out;
    
    // Parameter
    localparam  HALF_CYC_PERIOD = 5;

	// Instantiate the Unit Under Test (UUT)
	timeout_gen#(
        .TIMEOUT_THRESHOLD(5)
    ) 
    uut(
		.clk(clk), 
		.reset(reset),
        
		.timeout_out(timeout_out), 
        
        .slave_resp_valid_in(slave_resp_valid_in),
        
		.ad_cs_in(ad_cs_in), 
		.arbiter_resp_valid_in(arbiter_resp_valid_in) 
	);
    
    initial begin : clk_generation
        clk <= 0;
        #(HALF_CYC_PERIOD);
        forever begin
         #(HALF_CYC_PERIOD) clk = ~clk;
        end
    end

	initial begin
		// Initialize Inputs
		reset                   <= 1;
		ad_cs_in                <= 0;
		slave_resp_valid_in     <= 0;
        arbiter_resp_valid_in   <= 0;

		// Wait 100 ns for global reset to finish
		#100;
        
        @(negedge clk);
            reset <= 0;
        @(posedge clk);
            test_slave(0,10);
            test_slave(0,4);
            test_slave(0,5);
            test_slave(0,6);
            test_slave(0,7);
            test_slave(0,8);
            test_slave(1,10);
            test_slave(1,4);
            test_slave(1,5);
            test_slave(1,6);
            test_slave(1,7);
            test_slave(1,8);
            test_slave(2,10);
            test_slave(2,4);
            test_slave(2,5);
            test_slave(2,6);
            test_slave(2,7);
            test_slave(2,8);
        
        
		// Add stimulus here
	end
    
    always @(posedge clk) begin
        arbiter_resp_valid_in <= |slave_resp_valid_in;
    end
    
    task test_slave;
    input [1:0] slave_idx;
    input [7:0] number_of_clks_to_respond;
    
    begin : task_slave_main
        integer i;
        reg time_out_came = 0;
        
        case(slave_idx)
            2'd0 : begin
                @(posedge clk);
                ad_cs_in <= 3'b100;
                
                repeat(number_of_clks_to_respond) begin
                    @(posedge clk);
                    if (timeout_out == 1) begin
                        time_out_came <= 1'b1;
                        ad_cs_in      <= 3'b000;
                    end  
                end
                
                if(time_out_came == 0) begin
                    slave_resp_valid_in <= 3'b100;
                end
                else begin
                    time_out_came <= 0;
                end
                @(posedge clk);
                slave_resp_valid_in <= 3'b000;
                ad_cs_in            <= 3'b000;
                
            end
            2'd1 : begin
                @(posedge clk);
                ad_cs_in <= 3'b010;
                
                repeat(number_of_clks_to_respond) begin
                    @(posedge clk);
                    if (timeout_out == 1) begin
                        time_out_came <= 1'b1;
                        ad_cs_in      <= 3'b000;
                    end  
                end
                
                if(time_out_came == 0) begin
                    slave_resp_valid_in <= 3'b010;
                end
                else begin
                    time_out_came <= 0;
                end
                @(posedge clk);
                slave_resp_valid_in <= 3'b000;
                ad_cs_in            <= 3'b000;
            end
            2'd2 : begin
                @(posedge clk);
                ad_cs_in <= 3'b001;
                
                repeat(number_of_clks_to_respond) begin
                    @(posedge clk);
                    if (timeout_out == 1) begin
                        time_out_came <= 1'b1;
                        ad_cs_in      <= 3'b000;
                    end  
                end
                
                if(time_out_came == 0) begin
                    slave_resp_valid_in <= 3'b001;
                end
                else begin
                    time_out_came <= 0;
                end
                
                @(posedge clk);
                slave_resp_valid_in <= 3'b000;
                ad_cs_in            <= 3'b000;
            end
            default : begin
                @(posedge clk);
                ad_cs_in <= 3'b000;
                
                $display(" invalid slave_idx !! ");
            end
        endcase
        
        
      
    end
    
    
    
    
    endtask
    
    
      
endmodule

