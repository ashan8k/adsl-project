

/* STYLE_NOTES :
 * Always include the timescale directive for verilog design files
 */
`timescale 1ns / 1ps

/* DESIGN_NOTES :
 * Maximum number of slaves supported = 3
 * Maximum number of memory locations for individual slave  = 8K
 * Supported total number of memory locations = 24K
 */

module addr_decoder
(
    clk,
    reset,
	
	//Arbiter interface
    arbiter_rdata_out,
//    arbiter_rdata_strobe_out,
    arbiter_resp_valid_out,
    arbiter_error_out,
    arbiter_wdata_in,
    arbiter_rwdata_strobe_in,
    arbiter_valid_in,
    arbiter_addr_in,
    arbiter_rw_in,
	
	
	//SLave Interface
    slave_addr_out,
    slave_wdata_out,
    slave_rwdata_strobe_out,
    slave_cs_out,
    slave_rw_out,    
    slave_rdata_in,
//    slave_rdata_strobe_in,    
    slave_resp_valid_in,    
    slave_error_in,
    
    //timeout module
    timeout_in

);

//------------------------------------------------
// Parameters
//------------------------------------------------

    localparam MAX_SLAVE_ADDR_LENGTH = SLAVE_1_ADDR_WIDTH;

    localparam STATE_IDLE    = 1;
    localparam STATE_SLAVE_1 = 2;// these states are defined so that when arbiter assert a particular slave, responds only to that slave
    localparam STATE_SLAVE_2 = 3;
    localparam STATE_SLAVE_3 = 4;
    localparam STATE_WAIT    = 5;


//------------------------------------------------
// Global Includes
//------------------------------------------------

`include "bus_global_params.v"


//------------------------------------------------
// I/O Signals
//-----------------------------------------------

    input                                                           clk;
    input                                                           reset;

    output reg  [MASTER_DATA_WIDTH - 1:0]                           arbiter_rdata_out;
//    output reg  [MASTER_DATA_WIDTH/8 - 1:0]                         arbiter_rdata_strobe_out;     
    output reg                                                      arbiter_resp_valid_out;
    output reg                                                      arbiter_error_out;
    input       [MASTER_DATA_WIDTH - 1:0]                           arbiter_wdata_in;
    input       [MASTER_DATA_WIDTH/8 - 1:0]                         arbiter_rwdata_strobe_in;       
    input                                                           arbiter_valid_in;
    input       [MASTER_ADDR_WIDTH - 1:0]                           arbiter_addr_in;    
    input                                                           arbiter_rw_in;
                    
    output reg  [MAX_SLAVE_ADDR_LENGTH - 1:0]                       slave_addr_out;
    output reg  [SLAVE_DATA_WIDTH - 1:0]                            slave_wdata_out;
    output reg  [SLAVE_DATA_WIDTH/8 - 1:0]                          slave_rwdata_strobe_out;  
    output reg  [NUMBER_OF_SLAVES - 1:0]                            slave_cs_out;
    output reg                                                      slave_rw_out;
    input       [SLAVE_DATA_WIDTH*NUMBER_OF_SLAVES - 1:0]           slave_rdata_in;
//    input       [(SLAVE_DATA_WIDTH/8)*NUMBER_OF_SLAVES - 1:0]       slave_rdata_strobe_in; 
    input       [NUMBER_OF_SLAVES - 1:0]                            slave_resp_valid_in;
    input       [NUMBER_OF_SLAVES - 1:0]                            slave_error_in;
    
    input                                                           timeout_in;

//-----------------------------------------------
// Internal Wires and Regs
//-----------------------------------------------  

    wire         [SLAVE_DATA_WIDTH-1:0]                              slave_rdata_in_int[NUMBER_OF_SLAVES-1:0];
//    wire         [SLAVE_DATA_WIDTH/8-1:0]                            slave_rdata_strobe_in_int[NUMBER_OF_SLAVES-1:0];     
    integer                                                          state;

    generate
        genvar i;
        for (i = 1 ; i < NUMBER_OF_SLAVES + 1 ; i = i + 1 ) begin : input_mapping
            assign slave_rdata_in_int[i-1]          = slave_rdata_in[i*SLAVE_DATA_WIDTH - 1:(i-1)*SLAVE_DATA_WIDTH];		// assigning to multi dimension wire for parameterized number of slaves
//            assign slave_rdata_strobe_in_int[i-1]   = slave_rdata_strobe_in[i*(SLAVE_DATA_WIDTH/8) - 1:(i-1)*(SLAVE_DATA_WIDTH/8)];
        end
    endgenerate

//-----------------------------------------------
// Implementation
//-----------------------------------------------  
    always @ (posedge clk or posedge reset) begin
        if(reset) begin
            slave_cs_out                    <= 3'b000;
            arbiter_resp_valid_out          <= 0;
            state 			                <= STATE_IDLE;
        end
        else begin
            case(state) 
                STATE_IDLE : begin
                    arbiter_resp_valid_out       <= 0;
                    arbiter_error_out            <= 0;
                    
                    if(arbiter_valid_in) begin
                        slave_wdata_out             <= arbiter_wdata_in;
                        slave_rwdata_strobe_out      <= arbiter_rwdata_strobe_in;
                        slave_rw_out                <= arbiter_rw_in;
                        if(arbiter_addr_in[MASTER_ADDR_WIDTH-1] == 1'b0) begin // 8k allocation
                            slave_cs_out            <= 3'b001;
                            slave_addr_out          <= arbiter_addr_in[SLAVE_1_ADDR_WIDTH-1:0];
                            state                   <= STATE_SLAVE_1;
                        end
                        else if(arbiter_addr_in[MASTER_ADDR_WIDTH-2] == 1'b1) begin //invalid area of memory
                            slave_cs_out                <= 3'b000;
                            arbiter_resp_valid_out      <= 1;
                            arbiter_error_out           <= 1;
                        end
                        else if(arbiter_addr_in[MASTER_ADDR_WIDTH-3] == 1'b0) begin
                            slave_cs_out                <= 3'b010;
                            slave_addr_out              <= arbiter_addr_in[SLAVE_2_ADDR_WIDTH-1:0];
                            state                       <= STATE_SLAVE_2;
                        end     
                        else if(arbiter_addr_in[MASTER_ADDR_WIDTH-3] == 1'b1) begin
                            slave_cs_out                <= 3'b100;
                            slave_addr_out              <= arbiter_addr_in[SLAVE_3_ADDR_WIDTH-1:0];
                            state                       <= STATE_SLAVE_3;
                        end
                        else begin // undesirable
                            slave_cs_out                <= 3'b000;
                            arbiter_resp_valid_out      <= 1;
                            arbiter_error_out           <= 1;
                        end
                    end
                end
                STATE_SLAVE_1 : begin
//                    slave_cs_out                <= 3'b000;
                    arbiter_resp_valid_out      <= 0;
                    arbiter_error_out           <= 0;
                    
                    if(slave_resp_valid_in[0]) begin
                        slave_cs_out                    <= 3'b000;
                        arbiter_error_out 	            <= slave_error_in[0];
                        arbiter_rdata_out	            <= slave_rdata_in_int[0];
//                        arbiter_rdata_strobe_out        <= slave_rdata_strobe_in_int[0];
                        arbiter_resp_valid_out          <= 1;
                        state                           <= STATE_WAIT;
                    end
                    else if(timeout_in) begin
                        slave_cs_out                    <= 3'b000;
                        state                           <= STATE_WAIT;
                    end
                end
                STATE_SLAVE_2 : begin
//                    slave_cs_out                        <= 3'b000;
                    arbiter_resp_valid_out              <= 0;
                    arbiter_error_out                   <= 0;
                    
                    if(slave_resp_valid_in[1]) begin
                        slave_cs_out                    <= 3'b000;
                        arbiter_error_out 	            <= slave_error_in[1];
                        arbiter_rdata_out	            <= slave_rdata_in_int[1];
//                        arbiter_rdata_strobe_out        <= slave_rdata_strobe_in_int[1];
                        arbiter_resp_valid_out          <= 1;
                        state                           <= STATE_WAIT;
                    end
                    else if(timeout_in) begin
                        slave_cs_out                    <= 3'b000;
                        state                           <= STATE_WAIT;
                    end
                end
                STATE_SLAVE_3 : begin
//                    slave_cs_out                        <= 3'b000;
                    arbiter_resp_valid_out              <= 0;
                    arbiter_error_out                   <= 0;
                    
                    if(slave_resp_valid_in[2]) begin
                        slave_cs_out                    <= 3'b000;
                        arbiter_error_out 	            <= slave_error_in[2];
                        arbiter_rdata_out	            <= slave_rdata_in_int[2];
//                        arbiter_rdata_strobe_out        <= slave_rdata_strobe_in_int[2];
                        arbiter_resp_valid_out          <= 1;
                        state                           <= STATE_WAIT;
                    end
                    else if(timeout_in) begin
                        slave_cs_out                    <= 3'b000;
                        state <= STATE_WAIT;
                    end
					
                end
                STATE_WAIT : begin
                    state <= STATE_IDLE;
                end
                default : begin
                    state                               <= STATE_IDLE;
                    slave_cs_out                        <= 3'b000;
                end
            endcase
        end
    end

endmodule
