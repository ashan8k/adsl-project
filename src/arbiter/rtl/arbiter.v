`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:47:31 07/17/2013 
// Design Name: 
// Module Name:    arbiter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module arbiter
    (
    clk,
    reset,


    //Master 1 Master 2 inputs
    m1_addr_in,
    m1_wdata_in,
	 m1_rwdata_strobe_in,
    m1_rw_in,
    m1_pr_in,
    m1_valid_in,

    m2_addr_in,
    m2_wdata_in,
	 m2_rwdata_strobe_in,
    m2_rw_in,
    m2_pr_in,
    m2_valid_in,

    //Address Decoder inputs
    ad_rdata_in,
//	 ad_rdata_strobe_in,
    ad_resp_valid_in,
    ad_error_in,

    //Master 1 Master 2 outputs
    m_rdata_out,
//	 m_rdata_strobe_out,
    m1_resp_valid_out,
    m2_resp_valid_out,
    m1_error_out,
    m2_error_out,

    //Address Decoder outputs
    ad_addr_out,
    ad_valid_out,
    ad_wdata_out,
	 ad_rwdata_strobe_out,
	 ad_rw_out,
	
	//Time out Signal
	timeout_in
    );

//-------------------------------------------------------------------------
//  Global Include header
//-------------------------------------------------------------------------
//`include "bus_global_params.v"
	localparam 								MASTER_ADDR_WIDTH = 14;
	localparam 								MASTER_DATA_WIDTH = 64;
//-------------------------------------------------------------------------
// Parameters
//-------------------------------------------------------------------------
	
	parameter								BUS_PARK 				= 4'b0000;
	parameter								MASTER_ONE_READ 		= 4'b0001;
	parameter								MASTER_ONE_WRITE 		= 4'b0010;
	parameter								MASTER_TWO_READ 		= 4'b0011;
	parameter								MASTER_TWO_WRITE 		= 4'b0100;	
	parameter								MASTER_ONE_READ_WAIT 	= 4'b0101;
	parameter								MASTER_ONE_WRITE_WAIT	= 4'b0110;
	parameter								MASTER_TWO_READ_WAIT 	= 4'b0111;
	parameter								MASTER_TWO_WRITE_WAIT 	= 4'b1000;
	parameter								WAIT_ONE_CYCLE			= 4'b1001;

//--------------------------------------------------------------------------
// I/O Signals
//--------------------------------------------------------------------------


    input                                   clk;
    input                                   reset;
        
    input       [MASTER_ADDR_WIDTH-1:0]     m1_addr_in;
    input       [MASTER_ADDR_WIDTH-1:0]     m2_addr_in;
    input       [MASTER_DATA_WIDTH-1:0]     m1_wdata_in;
	input       [(MASTER_DATA_WIDTH/8)-1:0] m1_rwdata_strobe_in;
    input       [MASTER_DATA_WIDTH-1:0]     m2_wdata_in;
	input       [(MASTER_DATA_WIDTH/8)-1:0] m2_rwdata_strobe_in;
    input                                   m1_rw_in;
    input                                   m2_rw_in;
    input                                   m1_pr_in;
    input                                   m2_pr_in;
    input                                   m1_valid_in;
    input                                   m2_valid_in;

	//Address decoder input    
    input       [MASTER_DATA_WIDTH-1:0]     ad_rdata_in;
//    input       [(MASTER_DATA_WIDTH/8)-1:0] ad_rdata_strobe_in;
    input                                   ad_resp_valid_in;
    input                                   ad_error_in;

    output reg  [MASTER_DATA_WIDTH-1:0]     m_rdata_out;
//    output reg  [(MASTER_DATA_WIDTH/8)-1:0] m_rdata_strobe_out;	
    output reg                              m1_resp_valid_out;
    output reg                              m2_resp_valid_out;
    output reg                              m1_error_out;
    output reg                              m2_error_out;

    output reg  [MASTER_ADDR_WIDTH-1:0]     ad_addr_out;
    output reg                              ad_valid_out;
    output reg  [MASTER_DATA_WIDTH-1:0]     ad_wdata_out;
    output reg  [(MASTER_DATA_WIDTH/8)-1:0] ad_rwdata_strobe_out;
	output reg                              ad_rw_out;
	
	//time out module inputs
	input 									timeout_in;
	
	
//--------------------------------------------------------------------------
// Locally defined data
//--------------------------------------------------------------------------
	reg			[3:0]						arbiter_states;
	reg										previous_trans_holder;	// 0 for master one 1 for master 2
	
	
	

	//Code Beginning
    always @(posedge clk or posedge reset) begin
        if(reset) begin
			m1_resp_valid_out <= 1'b0;
			m2_resp_valid_out <= 1'b0;
			m1_error_out <= 1'b0;
			m2_error_out <= 1'b0;
			
			ad_valid_out <= 1'b0;
			ad_rw_out <= 1'b0;
			
			//Directly assign the read bus because it is common for both masters
			m_rdata_out <= ad_rdata_in;
//			m_rdata_strobe_out <= ad_rdata_strobe_in;
			
			//assign error line
			m1_error_out <= ad_error_in;
			m2_error_out <= ad_error_in;
			
			previous_trans_holder <= 1'b1; //make previous transaction holder as master 2 for allow access master one first
            arbiter_states <= BUS_PARK; //Put arbiter in parking state
        end
		
		else begin
			case(arbiter_states)
			
				BUS_PARK: begin
					//free the valid lines
					m1_resp_valid_out <= 1'b0; 
					m2_resp_valid_out <= 1'b0;
					ad_valid_out <= 1'b0;
					
					m1_error_out <= 1'b0; //free the error lines
					m2_error_out <= 1'b0;
					
                    if (m1_valid_in && m2_valid_in) begin //both holds valid data
					
                        if( m1_pr_in == m2_pr_in  && previous_trans_holder == 1'b1) begin	//equal priority with master 2 previously use bus
							if(m1_rw_in == 1'b0) begin	//master one read
								arbiter_states <= MASTER_ONE_READ;
							end
							if(m1_rw_in == 1'b1) begin	//master one write
								arbiter_states <= MASTER_ONE_WRITE;
							end							
                        end
						
						else if( m1_pr_in == m2_pr_in  && previous_trans_holder == 1'b0) begin	//equal priority with master 1 previously use bus
							if(m2_rw_in == 1'b0) begin	//master two read
								arbiter_states <= MASTER_TWO_READ;
							end
							if(m2_rw_in == 1'b1) begin	//master two write
								arbiter_states <= MASTER_TWO_WRITE;
							end							
                        end
						
						else if(m1_pr_in == 1'b1) begin	//Master one high priority
							if(m1_rw_in == 1'b0) begin	//master one read
								arbiter_states <= MASTER_ONE_READ;
							end
							if(m1_rw_in == 1'b1) begin	//master one write
								arbiter_states <= MASTER_ONE_WRITE;
							end							
						end
						
						else if(m2_pr_in == 1'b1) begin //Master two high priority
							if(m2_rw_in == 1'b0) begin	//master two read
								arbiter_states <= MASTER_TWO_READ;
							end
							if(m2_rw_in == 1'b1) begin	//master two write
								arbiter_states <= MASTER_TWO_WRITE;
							end							
						end
                    end
                    else if(m1_valid_in) begin //only master one holds valid data
							if(m1_rw_in == 1'b0) begin	//master one read
								arbiter_states <= MASTER_ONE_READ;
							end
							if(m1_rw_in == 1'b1) begin	//master one write
								arbiter_states <= MASTER_ONE_WRITE;
							end	
                    end
                    else if(m2_valid_in) begin	//only master two holds valid data
							if(m2_rw_in == 1'b0) begin	//master two read
								arbiter_states <= MASTER_TWO_READ;
							end
							if(m2_rw_in == 1'b1) begin	//master two write
								arbiter_states <= MASTER_TWO_WRITE;
							end		
                    end
                    else begin	//no one holds valid data
                        arbiter_states <= BUS_PARK;
                    end					
				end
				
				MASTER_ONE_READ: begin
					ad_addr_out <= m1_addr_in; //connect address bus
					ad_valid_out <= 1'b1; //say address decoder that data is valid
					ad_rw_out <= 1'b0; //say address decoder this is read
					m_rdata_out <= ad_rdata_in;	//transfer read data
//					m_rdata_strobe_out <= ad_rdata_strobe_in;	//transfer read data
					m1_error_out <= ad_error_in; //error line
					previous_trans_holder <= 1'b0; //master one transaction
					arbiter_states <= MASTER_ONE_READ_WAIT; //put arbiter in wait state				
				end
				
				MASTER_ONE_READ_WAIT: begin
					//when time out signal comes when transaction happens			
					if(timeout_in == 1'b1) begin
						m1_error_out <= 1'b1;
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //Lajan : fixed bug
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
					end
					//when address decoder holds valid data
					else if(ad_resp_valid_in == 1'b1 && ad_error_in == 1'b0) begin
						m_rdata_out <= ad_rdata_in;	//transfer read data
//						m_rdata_strobe_out <= ad_rdata_strobe_in;	//transfer read data
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //free the address decoder valid line
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
					end
					
					else if(ad_error_in == 1'b1)begin
						m1_error_out <= ad_error_in; //error line
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //Lajan : fixed bug
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state						
					end
				end
				
				MASTER_ONE_WRITE: begin
					ad_addr_out <= m1_addr_in; //connect address bus
					ad_wdata_out <= m1_wdata_in;	//connect data bus
					ad_rwdata_strobe_out <= m1_rwdata_strobe_in;	//connect data bus
					ad_valid_out <= 1'b1; //say address decoder that data is valid
					ad_rw_out <= 1'b1; //say address decoder this is write
					m1_error_out <= ad_error_in; //error line
					previous_trans_holder <= 1'b0; //master one transaction
					arbiter_states <= MASTER_ONE_WRITE_WAIT; //put arbiter in cycling state				
				end

				MASTER_ONE_WRITE_WAIT: begin
					//when time out signal comes when transaction happens			
					if(timeout_in == 1'b1) begin
						m1_error_out <= 1'b1;
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
						ad_valid_out <= 1'b0; //Lajan : fixed bug
					end
					//when address decoder holds valid data
					else if(ad_resp_valid_in == 1'b1 && ad_error_in == 1'b0) begin
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //free the address decoder valid line
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
					end
					
					else if(ad_error_in == 1'b1)begin
						m1_error_out <= ad_error_in; //error line
						m1_resp_valid_out <= 1'b1; //say master resource are valid
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
						ad_valid_out <= 1'b0; //Lajan : fixed bug						
					end				
				end	

				MASTER_TWO_READ: begin
					ad_addr_out <= m2_addr_in; //connect address bus
					ad_valid_out <= 1'b1; //say address decoder that data is valid
					ad_rw_out <= 1'b0; //say address decoder this is read
					m_rdata_out <= ad_rdata_in;	//transfer read data
//					m_rdata_strobe_out <= ad_rdata_strobe_in;	//transfer read data
					m2_error_out <= ad_error_in; //error line
					previous_trans_holder <= 1'b1; //master two transaction
					arbiter_states <= MASTER_TWO_READ_WAIT; //put arbiter in wait state					
				end
				
				MASTER_TWO_READ_WAIT: begin
					//when time out signal comes when transaction happens			
					if(timeout_in == 1'b1) begin
						m2_error_out <= 1'b1;
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
						ad_valid_out <= 1'b0; //Lajan : fixed bug
					end
					//when address decoder holds valid data
					else if(ad_resp_valid_in == 1'b1 && ad_error_in == 1'b0) begin
						m_rdata_out <= ad_rdata_in;	//transfer read data
//						m_rdata_strobe_out <= ad_rdata_strobe_in;	//transfer read data
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //free the address decoder valid line
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
					end
					
					else if(ad_error_in == 1'b1)begin
						m2_error_out <= ad_error_in; //error line	
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //Lajan : fixed bug
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state						
					end				
				end
				
				MASTER_TWO_WRITE: begin
					ad_addr_out <= m2_addr_in; //connect address bus
					ad_wdata_out <= m2_wdata_in;	//connect data bus
					ad_rwdata_strobe_out <= m2_rwdata_strobe_in;	//connect data bus
					ad_valid_out <= 1'b1; //say address decoder that data is valid
					ad_rw_out <= 1'b1; //say address decoder this is write
					m2_error_out <= ad_error_in; //error line
					previous_trans_holder <= 1'b1; //master two transaction
					arbiter_states <= MASTER_TWO_WRITE_WAIT; //put arbiter in cycling state				
				end

				MASTER_TWO_WRITE_WAIT: begin
					//when time out signal comes when transaction happens			
					if(timeout_in == 1'b1) begin
						m2_error_out <= 1'b1;
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //Lajan : fixed bug
					end
					//when address decoder holds valid data
					else if(ad_resp_valid_in == 1'b1 && ad_error_in == 1'b0) begin
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //free the address decoder valid line
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state
					end
					
					else if(ad_error_in == 1'b1)begin
						m2_error_out <= ad_error_in; //error line
						m2_resp_valid_out <= 1'b1; //say master resource are valid
						ad_valid_out <= 1'b0; //Lajan : fixed bug
						arbiter_states <= WAIT_ONE_CYCLE; //put arbiter in parking state						
					end					
				end				
				
				WAIT_ONE_CYCLE: begin
					arbiter_states <= BUS_PARK;
				end		
				
				
			endcase
		
		
		end
	end

endmodule




