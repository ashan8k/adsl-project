`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:47:31 07/17/2013 
// Design Name: 
// Module Name:    arbiter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bus_09(

    clk,
    reset,
    
    //Master 1 interface
    m1_addr_in,
    m1_wdata_in,
    m1_rwdata_strobe_in,
    m1_rw_in,
    m1_pr_in,
    m1_valid_in,
    m1_rdata_out,
    m1_resp_valid_out,
    m1_error_out,
    
    
    //Master 2 interface
    m2_addr_in,
    m2_wdata_in,
    m2_rwdata_strobe_in,
    m2_rw_in,
    m2_pr_in,
    m2_valid_in,
    m2_rdata_out,
    m2_resp_valid_out,
    m2_error_out,
   
    //Slave interface
    slave_rdata_in,
    slave_resp_valid_in,
    slave_error_in,
    slave_addr_out,
    slave_wdata_out,
    slave_rwdata_strobe_out,
    slave_cs_out,
    slave_rw_out

    );

//------------------------------------------------
// Global Includes
//------------------------------------------------

    `include "bus_global_params.v"

//------------------------------------------------
// Parameters
//------------------------------------------------

    parameter TIMEOUT_THRESHOLD      = 20;
    localparam MAX_SLAVE_ADDR_LENGTH = SLAVE_1_ADDR_WIDTH;

//-----------------------------------------------
// I/O Signals
//-----------------------------------------------


    input 										                clk;
    input										                reset;
    
    input 		[MASTER_ADDR_WIDTH - 1:0]		                m1_addr_in;
    input 		[MASTER_DATA_WIDTH - 1:0]		                m1_wdata_in;
    input 		[MASTER_DATA_WIDTH/8 - 1:0]		                m1_rwdata_strobe_in;     
    input 										                m1_rw_in;
    input										                m1_valid_in;
    input										                m1_pr_in;
    output		[MASTER_DATA_WIDTH - 1:0]		                m1_rdata_out;
    output										                m1_resp_valid_out;
    output										                m1_error_out;
    
    input 		[MASTER_ADDR_WIDTH - 1:0]		                m2_addr_in;
    input 		[MASTER_DATA_WIDTH - 1:0]		                m2_wdata_in;
    input 		[MASTER_DATA_WIDTH/8 - 1:0]		                m2_rwdata_strobe_in; 
    input 										                m2_rw_in;
    input										                m2_valid_in;
    input										                m2_pr_in;
    output		[MASTER_DATA_WIDTH - 1:0]		                m2_rdata_out;
    output										                m2_resp_valid_out;
    output										                m2_error_out;
    
    input		[SLAVE_DATA_WIDTH*NUMBER_OF_SLAVES - 1:0]		slave_rdata_in;
    input		[NUMBER_OF_SLAVES - 1:0]		                slave_resp_valid_in;
    input		[NUMBER_OF_SLAVES - 1:0]		                slave_error_in;	
    output		[MAX_SLAVE_ADDR_LENGTH - 1:0]	                slave_addr_out;
    output		[SLAVE_DATA_WIDTH - 1:0]		                slave_wdata_out;
    output      [SLAVE_DATA_WIDTH/8 - 1:0]		                slave_rwdata_strobe_out;
    output		[NUMBER_OF_SLAVES -1:0]			                slave_cs_out;
    output                                                      slave_rw_out;
    
//-----------------------------------------------
// Internal Wires and Registers    
//-----------------------------------------------
    //shared I/O
    wire        [MASTER_DATA_WIDTH - 1:0]       m_rdata_common_int;
    
    //global_timeout
    wire                                        timeout;
    
    
    //arbiter-address_decoder interconnect
    wire    [MASTER_DATA_WIDTH - 1:0]           arbiter_ad_rdata;
//    wire    [MASTER_DATA_WIDTH/8 - 1:0]         arbiter_ad_rdata_strobe;
    wire                                        arbiter_ad_resp_valid;
    wire                                        arbiter_ad_error;
    wire    [MASTER_ADDR_WIDTH - 1:0]           arbiter_ad_addr;
    wire                                        arbiter_ad_valid;
    wire    [MASTER_DATA_WIDTH - 1:0]           arbiter_ad_wdata;
    wire    [MASTER_DATA_WIDTH/8 - 1:0]         arbiter_ad_rwdata_strobe;
    wire                                        arbiter_ad_rw;
    
    //address_decoder-timeout_gen interconnect
    wire                                        ad_timeoutgen_resp_valid;
    
    
   
//-----------------------------------------------
// Implementation
//-----------------------------------------------
   
   assign m1_rdata_out = m_rdata_common_int;
   assign m2_rdata_out = m_rdata_common_int;
   
   arbiter arbiter_block
    (
        .clk                        (clk),
        .reset                      (reset),


        //Master 1 Master 2 inputs
        .m1_addr_in                 (m1_addr_in),
        .m1_wdata_in                (m1_wdata_in),
        .m1_rwdata_strobe_in         (m1_rwdata_strobe_in),
        .m1_rw_in                   (m1_rw_in),
        .m1_pr_in                   (m1_pr_in),
        .m1_valid_in                (m1_valid_in),
        
        .m2_addr_in                 (m2_addr_in),
        .m2_wdata_in                (m2_wdata_in),
        .m2_rwdata_strobe_in        (m2_rwdata_strobe_in),
        .m2_rw_in                   (m2_rw_in),
        .m2_pr_in                   (m2_pr_in),
        .m2_valid_in                (m2_valid_in),

        //Address Decoder inputs
        .ad_rdata_in                (arbiter_ad_rdata),
//        .ad_rdata_strobe_in         (arbiter_ad_rdata_strobe),
        .ad_resp_valid_in           (arbiter_ad_resp_valid),
        .ad_error_in                (arbiter_ad_error),

        //Master 1 Master 2 outputs
        .m_rdata_out                (m_rdata_common_int),
//        .m_rdata_strobe_out         (),
        .m1_resp_valid_out          (m1_resp_valid_out),
        .m2_resp_valid_out          (m2_resp_valid_out),
        .m1_error_out               (m1_error_out),
        .m2_error_out               (m2_error_out),

        //Address Decoder outputs
        .ad_addr_out                (arbiter_ad_addr),
        .ad_valid_out               (arbiter_ad_valid),
        .ad_wdata_out               (arbiter_ad_wdata),
        .ad_rwdata_strobe_out       (arbiter_ad_rwdata_strobe),
        .ad_rw_out                  (arbiter_ad_rw),
        
        //Time out Signal
        .timeout_in                 (timeout)
    );
    
    
    addr_decoder addr_decoder_block
    (
        .clk                        (clk),
        .reset                      (reset),
        
        //Arbiter interface
        .arbiter_rdata_out          (arbiter_ad_rdata),
        .arbiter_resp_valid_out     (arbiter_ad_resp_valid),
        .arbiter_error_out          (arbiter_ad_error),
        .arbiter_wdata_in           (arbiter_ad_wdata),
        .arbiter_rwdata_strobe_in   (arbiter_ad_rwdata_strobe),
        .arbiter_valid_in           (arbiter_ad_valid),
        .arbiter_addr_in            (arbiter_ad_addr),
        .arbiter_rw_in              (arbiter_ad_rw),
        
        
        //SLave Interface
        .slave_addr_out             (slave_addr_out),
        .slave_wdata_out            (slave_wdata_out),
        .slave_rwdata_strobe_out    (slave_rwdata_strobe_out),
        .slave_cs_out               (slave_cs_out),
        .slave_rw_out               (slave_rw_out),    
        .slave_rdata_in             (slave_rdata_in),  
        .slave_resp_valid_in        (slave_resp_valid_in),    
        .slave_error_in             (slave_error_in),
        
        //timeout module
        .timeout_in                 (timeout)

    );
    
    timeout_gen
    #(
        .TIMEOUT_THRESHOLD          (TIMEOUT_THRESHOLD)
    )
    timeout_gen_block
    (
        .clk                        (clk),
        .reset                      (reset),
        
        //Arbiter interface
        .timeout_out                (timeout),
       
        //SLave Interface
        .slave_resp_valid_in        (slave_resp_valid_in),
        
        //Address_decoder
        .arbiter_resp_valid_in      (arbiter_ad_resp_valid),
        .ad_cs_in                   (slave_cs_out)
        //timeout_out,                     ** same signal as above

    );


endmodule