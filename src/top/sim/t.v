`timescale 1ns / 1ps
  

module tb;

    `include "bus_global_params.v"
    parameter MAX_SLAVE_ADDR_LENGTH = 13;

    reg [MASTER_ADDR_WIDTH-1:0] addr1, addr2;
    reg [MASTER_DATA_WIDTH-1:0] wdata1, wdata2;
    reg rw1, rw2;
    reg pr1, pr2;
    reg valid1, valid2;
    reg [7:0] rwdata_strobe1, rwdata_strobe2;

    wire res_valid1, res_valid2;
    wire error1, error2;
    wire [MASTER_DATA_WIDTH-1:0] rdata1, rdata2;
    
    wire [SLAVE_DATA_WIDTH*NUMBER_OF_SLAVES - 1:0]		slave_rdata;
    wire [NUMBER_OF_SLAVES - 1:0]		                slave_resp_valid;
    wire [NUMBER_OF_SLAVES - 1:0]		                slave_error;	
    wire [MAX_SLAVE_ADDR_LENGTH - 1:0]	                        slave_addr;
    wire [SLAVE_DATA_WIDTH - 1:0]		                slave_wdata;
    wire [SLAVE_DATA_WIDTH/8 - 1:0]		                slave_rwdata_strobe;
    wire [NUMBER_OF_SLAVES -1:0]			        slave_cs;
    wire                                                        slave_rw;
 
    reg clk, reset;
    reg rand_valid1, rand_valid2;
    reg [2:0] rand_delay1, rand_delay2;
    integer cnt,cnt1,cnt2;

    reg mark[1<<14:0];
    integer matches1 = 0;
    integer matches2 = 0;

    reg done2;
    reg done1;
    reg start = 0;

    always #10 clk = ~clk;

    initial begin
      start = 0;
      clk = 0;
      rwdata_strobe1 = 8'hff;
      rwdata_strobe2 = 8'hff;
      valid1 = 0;
      valid2 = 0;
      M(0,0,0,1,0,0);
      M(1,0,0,1,0,0);
      //mark[0] = 0;
      //for (cnt = 1; cnt != 1<<14; cnt = cnt + 1) begin
      //  mark[cnt] = 0;
      //end
      //start = 1;
      ////reset check
      //#105 reset = 1;
      //#105 reset = 0;
      //#100 $finish;

      /****************************************
      TESTCASES COMMENTED OUT
      UNCOMMENT RELEVANT TESTCASE TO SIMULATE
      ****************************************/

      //// Check valid signal high intact
      //// Master 1 write
      //M(0,14'h0001,64'h0001,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#200

      ////Simple read & write
      //// Master 1 write
      //M(0,14'h0041,64'h0019,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //rw2 = 0;
      //#100
      ////Master 1 read
      //M(0,14'h0041,64'h0019,0,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;

      //#200

      //M(1,14'h2fff,64'h1234,1,0,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //rw1 = 0;
      //#100
      //// Master 1 read
      //M(1,14'h2fff,64'h1234,0,0,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#1000
  
      // Access invalid address
      //M(0,14'h3fff,64'h2345,0,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#1000 $finish;

  
      //// Valid random addresses to each of the slaves
      //M(0,14'h1fff,64'h1335,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#100
      //M(1,14'h1ff1,64'h1335,1,0,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#100
      //M(0,14'h22ff,64'h1325,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#100
      //M(0,14'h2fff,64'h2345,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#100
      //M(1,14'h2ffe,64'h2345,1,0,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#200


      //Masters transaction request overlap
      //M(0,14'h0003,64'h0003,1,0,1);
      //#10;
      //M(1,14'h0004,64'h0004,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //M(0,14'h0003,64'h2345,0,0,1);
      //assertEquality(rdata2,4'h0003);
      //@(posedge res_valid1);
      //M(1,14'h0004,64'h2345,0,0,1);
      //assertEquality(rdata1,4'h0004);
      //@(posedge clk)
      //valid1 = 0;


      //#100
      //valid1 = 0; valid2 = 0;
      ////Masters transaction request overlap - hold valid
      //M(0,14'h0003,64'h0003,1,0,1);
      //#10;
      //M(1,14'h0004,64'h0004,1,0,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //M(0,14'h0003,64'h2345,0,0,1);
      //assertEquality(rdata2,4'h0003);
      //@(posedge res_valid1);
      //M(1,14'h0004,64'h2345,0,0,1);
      //assertEquality(rdata1,4'h0004);
      //@(posedge clk)
      //valid1 = 0;


      //#100
      //valid1 = 0; valid2 = 0;
      ////Masters transaction request overlap - high priority throughout
      //M(0,14'h0003,64'h0003,1,1,1);
      //#10;
      //M(1,14'h0004,64'h0004,1,0,1);
      //#1000

      //#100
      //valid1 = 0; valid2 = 0;
      ////Masters transaction request overlap - high priority throughout, check valid signal
      //M(0,14'h0003,64'h0003,1,1,1);
      //#10;
      //M(1,14'h0004,64'h0004,1,0,1);
      //@(posedge res_valid2) 
      //valid2 = 0;
      //#1000

      //// Timeout
      //addr1 = 1; addr2 = 1;
      //wdata1 = 1; wdata2 = 1;
      //valid1 = 1;
      //pr1 = 0;
      //rw1 = 1;
      //valid2 = 1;
      //pr2 = 0;
      //rw2 = 0;
      //#1000
      //pr1 = 1;
      //#5000
      //$finish;



      ////Priority working ??
      //// Master 2 write
      //#3
      //M(1,14'h0002,64'h0002,1,1,1);
      //M(0,14'h0002,64'h0002,1,1,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid2 = 0;
      //#100
      //
      //M(1,14'h0002,64'h0002,1,1,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#100
    
      //#3
      //M(1,14'h0002,64'h0002,1,1,1);
      //M(0,14'h0002,64'h0002,1,1,1);
      //@(posedge res_valid2);
      //@(posedge clk)
      //valid1 = 0;
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid2 = 0;
      //#100
      //


      //M(1,14'h0002,64'h0001,1,0,1);
      //M(0,14'h0003,64'h0004,1,0,1);
      //@(posedge res_valid1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //@(posedge res_valid2);
      //@(posedge res_valid2);
      //@(posedge res_valid2);
      //M(1,14'h0002,64'h0001,0,1,1);
      //#1000


      //// Response after timeout
      //M(1,14'h0002,64'h0002,1,1,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      ////valid1 = 0;
      //@(slave_resp_valid)
      //valid1 = 0;

      //#300
      //
      //M(1,14'h0002,64'h0002,1,1,0);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#100

      ////Slave reports error
      //M(1,14'h0002,64'h0002,0,1,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#100
      //M(1,14'h0002,64'h0002,1,1,1);
      //@(posedge res_valid1);
      //@(posedge clk)
      //valid1 = 0;
      //#1000
    
      //MASTER CHECK
      //@(posedge done1);
      //if(!done2) begin
      //  @(posedge done2)
      //  $display("matches1 : %d\n",matches1);
      //  $display("matches2 : %d\n",matches2);
      //  $display("matches : %d\n",matches1+matches2);
      //  $finish;
      //end
      //$finish;


    end

    //MASTER CHECK
    //initial begin
    //  @(posedge start);
    //  done1 = 0;
    //  rw1 = 1;
    //  valid1 = 1;

    //  repeat(10000) begin
    //    rand_valid1 = $random;
    //    if(rand_valid1) begin
    //      valid1 = 0;
    //      rand_delay1 = $random;
    //      for (cnt1 = 0; cnt1 <= rand_delay1; cnt1 = cnt1 + 1) begin
    //        @(posedge clk);
    //      end
    //      valid1 = 1;
    //    end
    //    addr1 = $random;
    //    rw1 = $random;
    //    if(rw1) begin 
    //      wdata1 = addr1;
    //      mark[addr1] = 1;
    //      pr1 = $random;
    //      @(posedge res_valid1);
    //      @(posedge clk);
    //    end
    //    else begin
    //      if(mark[addr1]) begin
    //        pr1 = $random;
    //        @(posedge res_valid1);
    //        if(!error1) begin
    //        assertEquality(rdata1,addr1);
    //        matches1 = matches1 + 1;
    //        end
    //      end
    //    end
    //  end
    //  done1 = 1;
    //  valid1 = 0;
    //end

    //initial begin
    //  @(posedge start);
    //  done2 = 0;
    //  rw2 = 1;
    //  valid2 = 1;

    //  repeat(10000) begin
    //    rand_valid2 = $random;
    //    if(rand_valid2) begin
    //      valid2 = 0;
    //      rand_delay2 = $random;
    //      for (cnt2 = 0; cnt2 <= rand_delay2; cnt2 = cnt2 + 1) begin
    //        @(posedge clk);
    //      end
    //      valid2 = 1;
    //    end
    //    addr2 = $random;
    //    rw2 = $random;
    //    if(rw2) begin 
    //      wdata2 = addr2;
    //      mark[addr2] = 1;
    //      pr2 = $random;
    //      @(posedge res_valid2);
    //      @(posedge clk);
    //    end
    //    else begin
    //      if(mark[addr2]) begin
    //        pr2 = $random;
    //        @(posedge res_valid2);
    //        if(!error2) begin
    //        assertEquality(rdata2,addr2);
    //        matches2 = matches2 + 1;
    //        end
    //      end
    //    end
    //  end
    //  done2 = 1;
    //  valid2 = 0;
    //end 
    //always@(posedge clk)
    //  if(res_valid1)
    //    

    //always@(posedge clk)
    //  if(done) begin
    //    status0 = $fscanf(in,"%b %h %h %b %b %b\n",sel,addr_,wdata_,rw_,pr_,valid_);
    //    M(sel,addr_,wdata_,rw_,pr_,valid_);
    //  end


task assertEquality;
  input a;
  input b;
  if(a!=b) begin
    $display("assertion failed at %d\n",$time);
    $finish;
  end
  else begin
    $display("match \n");
  end
endtask


task M;
    input sel;
    input [MASTER_ADDR_WIDTH-1:0] addr_;
    input [MASTER_DATA_WIDTH-1:0] wdata_;
    input rw_;
    input pr_;
    input valid_;

    begin
      if(sel) begin
        addr1 = addr_;
        wdata1 = wdata_;
        rw1 = rw_;
        pr1 = pr_;
        valid1 = valid_;
        $display("M1\n");
      end
      else begin
        addr2 = addr_;
        wdata2 = wdata_;
        rw2 = rw_;
        pr2 = pr_;
        valid2 = valid_;
        $display("M2\n");
      end
    end
endtask

//task M;
//    input [MASTER_ADDR_WIDTH-1:0] addr_1,addr_2;
//    input [MASTER_DATA_WIDTH-1:0] wdata_1,wdata_2;
//    input rw_1,rw_2;
//    input pr_1,pr_2;
//    input valid_1,valid_2;
//
//    begin
//      @(posedge clk) begin
//        addr1 <= addr_;
//        wdata1 <= wdata_;
//        rw1 <= rw_;
//        pr1 <= pr_;
//        valid1 <= valid_;
//        $display("M1\n");
//      end
//    end
//endtask

bus_09 bus(

    .clk  (clk),
    .reset(reset),
    
    //Master 1 Master 2 inputs
    .m1_addr_in  (addr1),
    .m2_addr_in  (addr2),
    .m1_wdata_in(wdata1),
    .m2_wdata_in(wdata2),
    .m1_rw_in    (rw1),
    .m2_rw_in    (rw2),
    .m1_pr_in    (pr1),
    .m2_pr_in    (pr2),
    .m1_valid_in (valid1),
    .m2_valid_in (valid2),



    //Master 1 Master 2 outputs
    .m1_rdata_out   (rdata1),
    .m2_rdata_out   (rdata2),
    .m1_resp_valid_out(res_valid1),
    .m2_resp_valid_out(res_valid2),
    .m1_error_out    (error1),
    .m2_error_out    (error2),
    .m1_rwdata_strobe_in(rwdata_strobe1),
    .m2_rwdata_strobe_in(rwdata_strobe2),
   
    //Slave interface
    .slave_rdata_in          (slave_rdata         ),
    .slave_resp_valid_in     (slave_resp_valid    ), 
    .slave_error_in          (slave_error         ), 
    .slave_addr_out          (slave_addr         ), 
    .slave_wdata_out         (slave_wdata        ), 
    .slave_rwdata_strobe_out (slave_rwdata_strobe), 
    .slave_cs_out            (slave_cs           ), 
    .slave_rw_out(slave_rw)
    
);

slave #(SLAVE_1_ADDR_WIDTH,SLAVE_DATA_WIDTH)
slave1(
    .clk        (clk),
    .reset      (reset),
    .cs         (slave_cs[2]),
    .rw         (slave_rw),
    .addr       (slave_addr[SLAVE_1_ADDR_WIDTH-1:0]),
    .wdata      (slave_wdata),
    .rw_strobe  (slave_rwdata_strobe),
    .rdata      (slave_rdata[3*SLAVE_DATA_WIDTH-1:2*SLAVE_DATA_WIDTH]),
    .resp_valid (slave_resp_valid[2]),
    .error      (slave_error[2])
);
slave #(SLAVE_2_ADDR_WIDTH,SLAVE_DATA_WIDTH)
slave2(
    .clk        (clk),
    .reset      (reset),
    .cs         (slave_cs[1]),
    .rw         (slave_rw),
    .addr       (slave_addr[SLAVE_2_ADDR_WIDTH-1:0]),
    .wdata      (slave_wdata),
    .rw_strobe  (slave_rwdata_strobe),
    .rdata      (slave_rdata[2*SLAVE_DATA_WIDTH-1:SLAVE_DATA_WIDTH]),
    .resp_valid (slave_resp_valid[1]),
    .error      (slave_error[1])
);
slave #(SLAVE_3_ADDR_WIDTH,SLAVE_DATA_WIDTH)
slave3(
    .clk        (clk),
    .reset      (reset),
    .cs         (slave_cs[0]),
    .rw         (slave_rw),
    .addr       (slave_addr[SLAVE_3_ADDR_WIDTH-1:0]),
    .wdata      (slave_wdata),
    .rw_strobe  (slave_rwdata_strobe),
    .rdata      (slave_rdata[SLAVE_DATA_WIDTH-1:0]),
    .resp_valid (slave_resp_valid[0]),
    .error      (slave_error[0])
);

   
endmodule

module slave
    (
      clk   ,
      reset ,

      cs        ,
      rw        ,
      addr      ,
      wdata     ,
      rw_strobe ,

      rdata      ,
      resp_valid ,
      error
    );

    parameter SLAVE_ADDR_WIDTH = 14;
    parameter SLAVE_DATA_WIDTH = 64;
    
    input clk   ;
    input reset ;

    input cs					 ;
    input rw   					 ;
    input [SLAVE_ADDR_WIDTH-1:0] addr            ;
    input [SLAVE_DATA_WIDTH-1:0] wdata           ;
    input [(SLAVE_DATA_WIDTH>>3)-1:0] rw_strobe    ;

    output reg [SLAVE_DATA_WIDTH-1:0] rdata           ;
    output reg resp_valid                             ;
    output reg error                                  ;
    
    reg[SLAVE_DATA_WIDTH-1:0] memory[0:(1<<SLAVE_ADDR_WIDTH)-1];
    integer i; 
    always@(posedge clk)begin
	    if(reset)begin
                    resp_valid <= 1'b0;
                    error <= 1'b0;
		    for (i=0;i<(1<<SLAVE_ADDR_WIDTH);i=i+1)
			    memory[i] <= 64'b0;
	    end
	    else begin
	   	   if(cs)begin
		   	   if(rw)begin
					   if(rw_strobe[0])memory[addr][7:0]  =wdata[7:0];
					   if(rw_strobe[1])memory[addr][15:8] =wdata[15:8];
					   if(rw_strobe[2])memory[addr][23:16]=wdata[23:16];
					   if(rw_strobe[3])memory[addr][31:24]=wdata[31:24];
					   if(rw_strobe[4])memory[addr][39:32]=wdata[39:32];
					   if(rw_strobe[5])memory[addr][47:40]=wdata[47:40];
					   if(rw_strobe[6])memory[addr][55:48]=wdata[55:48];
					   if(rw_strobe[7])memory[addr][63:56]=wdata[63:56];
                                           if(rw_strobe[0]) $display("%h\n",memory[addr]);

		    	   end			    
		    	   else begin
					   if(rw_strobe[0])rdata[7:0]  =memory[addr][7:0];
				   	   if(rw_strobe[1])rdata[15:8] =memory[addr][15:8];
					   if(rw_strobe[2])rdata[23:16]=memory[addr][23:16];
					   if(rw_strobe[3])rdata[31:24]=memory[addr][31:24];
					   if(rw_strobe[4])rdata[39:32]=memory[addr][39:32];
					   if(rw_strobe[5])rdata[47:40]=memory[addr][47:40];
					   if(rw_strobe[6])rdata[55:48]=memory[addr][55:48];
					   if(rw_strobe[7])rdata[63:56]=memory[addr][63:56];
			  end
                          if(!rw && memory[addr] == 64'b0) endTransfer(5'd10,0);
                          else endTransfer(5'd10,0);
	    	   end
	    end
    end
    task endTransfer;
	input[4:0] delay;
	input err;
	    begin
		    repeat(delay)@(posedge clk);
		    @(posedge clk)begin
			    resp_valid <= 1'b1;
			    error <= err;
		    end
		    @(posedge clk)begin
			    resp_valid <= 1'b0;
		    end
	    end
    endtask

endmodule
