//Global header

localparam MASTER_ADDR_WIDTH = 14;
localparam MASTER_DATA_WIDTH = 64;

localparam SLAVE_1_ADDR_WIDTH  = 13;
localparam SLAVE_2_ADDR_WIDTH  = 11;
localparam SLAVE_3_ADDR_WIDTH  = 11;
localparam SLAVE_DATA_WIDTH    = 64;
localparam NUMBER_OF_SLAVES    = 3;
